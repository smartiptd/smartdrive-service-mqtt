#!/usr/bin/env node

/* Module dependencies */
var pkg = require('./package');
var mongoose = require('mongoose');
var logger = require('smartdrive-lib').logger;
var config = require('config'),
    dbconf = config.get('db');

function start() {
    try {
        /* Connect to database */
       mongoose.connect('mongodb://' + dbconf.host + '/' + dbconf.dbName);

        /* Start MQTT process */
        require('./broker').process();

    } catch (err) {
        logger.error(err);
    }
}

/* Start process */
start();
