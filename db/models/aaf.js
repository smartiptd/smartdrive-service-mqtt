'use strict';

/* Module dependencies */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/* Mongoose schema */
var systemStatusDataSehema = new Schema({
    subsysID: { type:Schema.Types.ObjectId, require:true },
    powerSourceInfo: { type:String },
    manualSwitch: { type:String },
    defaultFactory: { type:String },
    rawDataPv: { type:String },
    operateMode: { type:String },
    firmwareVersion: { type:String },
    srtvMode: { type:String },
    timestamp: { type:Date, require:true, default:new Date() },
    createDate: { type:Date, require:true, default:new Date() },
    updateDate: { type:Date, require:true, default:new Date() },
    timeOffset: { type:Number, require:true, default:new Date().getTimezoneOffset() }
});

var temperatureStatusDataSchema = new Schema({
    subsysID: { type:Schema.Types.ObjectId, require:true },
    ambientTemp: { type:String },
    atticTemp: { type:String },
    roomTemp: { type:String },
    timestamp: { type:Date, require:true, default:new Date() },
    createDate: { type:Date, require:true, default:new Date() },
    updateDate: { type:Date, require:true, default:new Date() },
    timeOffset: { type:Number, require:true, default:new Date().getTimezoneOffset() }
});

var solarCellStatusDataSchema = new Schema({
    subsysID: { type:Schema.Types.ObjectId, require:true },
    solarCellLevel: { type:String },
    solarCellVoltage: { type:String },
    solarCellCurrent: { type:String },
    solarCellPower: { type:String },
    timestamp: { type:Date, require:true, default:new Date() },
    createDate: { type:Date, require:true, default:new Date() },
    updateDate: { type:Date, require:true, default:new Date() },
    timeOffset: { type:Number, require:true, default:new Date().getTimezoneOffset() }
});

var sensorStatusDataSehema = new Schema({
    subsysID: { type:Schema.Types.ObjectId, require:true },
    livingSensorStatus: { type:String },
    ambientSensorStatus: { type:String },
    atticSensorStatus: { type:String },
    timestamp: { type:Date, require:true, default:new Date() },
    createDate: { type:Date, require:true, default:new Date() },
    updateDate: { type:Date, require:true, default:new Date() },
    timeOffset: { type:Number, require:true, default:new Date().getTimezoneOffset() }
});

var ventilatorStatusDataSchema = new Schema({
    subsysID: { type:Schema.Types.ObjectId, require:true },
    srtvMain: { type:String },
    cvMain: { type:String },
    timestamp: { type:Date, require:true, default:new Date() },
    createDate: { type:Date, require:true, default:new Date() },
    updateDate: { type:Date, require:true, default:new Date() },
    timeOffset: { type:Number, require:true, default:new Date().getTimezoneOffset() }
});

var controlHistoryDataSchema = new Schema({
    subsysID: { type:Schema.Types.ObjectId, require:true },
    userID: { type:Schema.Types.ObjectId, require:true },
    systemMode: { type:String },
    srtvMain: { type:String },
    cvMain: { type:String },
    timestamp: { type:Date, require:true, default:new Date() },
    createDate: { type:Date, require:true, default:new Date() },
    updateDate: { type:Date, require:true, default:new Date() },
    timeOffset: { type:Number, require:true, default:new Date().getTimezoneOffset() }
});

/* Schema middleware */
systemStatusDataSehema.pre('update', function() {
    this.update({},{$set: {updateDate:new Date()}});
});

temperatureStatusDataSchema.pre('update', function() {
    this.update({},{$set: {updateDate:new Date()}});
});

solarCellStatusDataSchema.pre('update', function() {
    this.update({},{$set: {updateDate:new Date()}});
});

sensorStatusDataSehema.pre('update', function() {
    this.update({},{$set: {updateDate:new Date()}});
});

ventilatorStatusDataSchema.pre('update', function() {
    this.update({},{$set: {updateDate:new Date()}});
});

controlHistoryDataSchema.pre('update', function() {
    this.update({},{$set: {updateDate:new Date()}});
});

/* Export model */
exports.SystemStatusData = mongoose.model('aaf.SystemStatusData', systemStatusDataSehema);
exports.TemperatureStatusData = mongoose.model('aaf.TemperatureStatusData', temperatureStatusDataSchema);
exports.SolarCellStatusData = mongoose.model('aaf.SolarCellStatusData', solarCellStatusDataSchema);
exports.SensorStatusData = mongoose.model('aaf.SensorStatusData', sensorStatusDataSehema);
exports.VentilatorStatusData = mongoose.model('aaf.VentilatorStatusData', ventilatorStatusDataSchema);
exports.ControlHistoryData = mongoose.model('aaf.ControlHistoryData', controlHistoryDataSchema);
