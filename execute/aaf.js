'use strict';

/* Module dependencies */
var logger = require('smartdrive-lib').logger;
var constant = require('smartdrive-lib').constant;
var request = require('request');
var pkg = require('../package');
var config = require('config'),
    conf = config.get('app');
var db = require('../db');

/**
* Active AIRflow(TM) process
*
* @param {Object} broker
* @param {Object} client
* @param {Object} packet
* @param {Object} info
*/
module.exports = function(broker, client, packet, info) {
    try {
        if (info.path) {
            var path = constant.SLASH + info.path.toString().replace(/[\,]/g, '\/');
            switch (path) {

                /* Save system status data */
                case conf.broker.topic.aaf.path.systemStatus:
                    var _payload = JSON.parse(packet.payload.toString());
                    if (_payload && _payload.status.system) {
                        var _data = _payload.status.system;
                        var _systemStatusData = new db.AAF.SystemStatusData();
                        _systemStatusData.subsysID = db.parseObjectId(_data.subsysID);
                        _systemStatusData.powerSourceInfo = _data.powerSourceInfo;
                        _systemStatusData.manualSwitch = _data.manualSwitch;
                        _systemStatusData.defaultFactory = _data.defaultFactory;
                        _systemStatusData.rawDataPv = _data.rawDataPv;
                        _systemStatusData.operateMode = _data.operateMode;
                        _systemStatusData.firmwareVersion = _data.firmwareVersion;
                        _systemStatusData.srtvMode = _data.srtvMode;
                        _systemStatusData.timestamp = _data.timestamp;
                        _systemStatusData.save(function(err) {
                            if (err) {
                                logger.error(err);
                            } else {
                                logger.data(_data);
                            }
                        });
                    }
                    break;

                /* Save temperature status data */
                case conf.broker.topic.aaf.path.temperatureStatus:
                    var _payload = JSON.parse(packet.payload.toString());
                    if (_payload && _payload.status.temperature) {
                        var _data = _payload.status.temperature;
                        var _temperatureStatusData = new db.AAF.TemperatureStatusData();
                        _temperatureStatusData.subsysID = db.parseObjectId(_data.subsysID);
                        _temperatureStatusData.ambientTemp = _data.ambientTemp;
                        _temperatureStatusData.atticTemp = _data.atticTemp;
                        _temperatureStatusData.roomTemp = _data.roomTemp;
                        _temperatureStatusData.timestamp = _data.timestamp;
                        _temperatureStatusData.save(function(err) {
                            if (err) {
                                logger.error(err);
                            } else {
                                logger.data(_data);
                            }
                        });
                    }
                    break;

                /* Save solar cell status data */
                case conf.broker.topic.aaf.path.solarCellStatus:
                    var _payload = JSON.parse(packet.payload.toString());
                    if (_payload && _payload.status.solarCell) {
                        var _data = _payload.status.solarCell;
                        var _solarCellStatusData = new db.AAF.SolarCellStatusData();
                        _solarCellStatusData.subsysID = db.parseObjectId(_data.subsysID);
                        _solarCellStatusData.solarCellLevel = _data.solarCellLevel;
                        _solarCellStatusData.solarCellVoltage = _data.solarCellVoltage;
                        _solarCellStatusData.solarCellCurrent = _data.solarCellCurrent;
                        _solarCellStatusData.solarCellPower = _data.solarCellPower;
                        _solarCellStatusData.timestamp = _data.timestamp;
                        _solarCellStatusData.save(function(err) {
                            if (err) {
                                logger.error(err);
                            } else {
                                logger.data(_data);
                            }
                        });
                    }
                    break;

                /* Save sensor status data */
                case conf.broker.topic.aaf.path.sensorStatus:
                    var _payload = JSON.parse(packet.payload.toString());
                    if (_payload && _payload.status.sensor) {
                        var _data = _payload.status.sensor;
                        var _sensorStatusData = new db.AAF.SensorStatusData();
                        _sensorStatusData.subsysID = db.parseObjectId(_data.subsysID);
                        _sensorStatusData.livingSensorStatus = _data.livingSensorStatus;
                        _sensorStatusData.ambientSensorStatus = _data.ambientSensorStatus;
                        _sensorStatusData.atticSensorStatus = _data.atticSensorStatus;
                        _sensorStatusData.timestamp = _data.timestamp;
                        _sensorStatusData.save(function(err) {
                            if (err) {
                                logger.error(err);
                            } else {
                                logger.data(_data);
                            }
                        });
                    }
                    break;

                /* Save ventilator status data */
                case conf.broker.topic.aaf.path.ventilatorStatus:
                    var _payload = JSON.parse(packet.payload.toString());
                    if (_payload && _payload.status.ventilator) {
                        var _data = _payload.status.ventilator;
                        var _ventilatorStatusData = new db.AAF.VentilatorStatusData();
                        _ventilatorStatusData.subsysID = db.parseObjectId(_data.subsysID);
                        _ventilatorStatusData.srtvMain = _data.srtvMain;
                        _ventilatorStatusData.cvMain = _data.cvMain;
                        _ventilatorStatusData.timestamp = _data.timestamp;
                        _ventilatorStatusData.save(function(err) {
                            if (err) {
                                logger.error(err);
                            } else {
                                logger.data(_data);
                            }
                        });
                    }
                    break;

                /* Save control history data */
                case conf.broker.topic.aaf.path.ventilatorControl:
                    var _payload = JSON.parse(packet.payload.toString());
                    if (_payload && _payload.control.ventilator) {
                        var _data = _payload.control.ventilator;
                        var _controlHistoryData = new db.AAF.ControlHistoryData();
                        _controlHistoryData.subsysID = db.parseObjectId(_data.subsysID);
                        _controlHistoryData.userID = db.parseObjectId(_data.userID);
                        _controlHistoryData.systemMode = _data.systemMode;
                        _controlHistoryData.srtvMain = _data.srtvMain;
                        _controlHistoryData.cvMain = _data.cvMain;
                        _controlHistoryData.timestamp = _data.timestamp;
                        _controlHistoryData.save(function(err) {
                            if (err) {
                                logger.error(err);
                            } else {
                                logger.data(_data);
                            }
                        });
                    }
                    break;

                default:
                    break;
            }
        }
    } catch (err) {
        logger.error(err);
    }
}
