'use strict';

/* Module dependencies */
var logger = require('smartdrive-lib').logger;
var constant = require('smartdrive-lib').constant;
var request = require('request');
var pkg = require('../package');
var config = require('config'),
    conf = config.get('app');

/**
* Common process
*
* @param {Object} broker
* @param {Object} client
* @param {Object} packet
* @param {Object} info
*/
module.exports = function(broker, client, packet, info) {
    try {
        if (info.path) {
            var path = constant.SLASH + info.path.toString().replace(/[\,]/g, '\/');
            switch (path) {

                /* Challenge broker */
                case conf.broker.topic.common.path.challengeBroker:
                    var _payload = JSON.parse(packet.payload.toString());
                    if (_payload && _payload.challengeText) {
                        var options = {
                            method: 'GET',
                            url: conf.oauth.uri.challengeBroker,
                            rejectUnauthorized: conf.oauth.options.rejectUnauthorized,
                            qs: {
                                access_token: broker.token.access_token,
                                challenge_text: _payload.challengeText
                            },
                            headers: { 'cache-control': 'no-cache' }
                        };
                        request(options, function (err, res, _body) {
                            var _challenge = JSON.parse(_body);
                            var message = {
                                topic: packet.topic + '/reply',
                                qos: 0,
                                retain: false
                            };
                            message.payload = (err || !_challenge || !_challenge.challenge_code) ?
                            JSON.stringify({}) : JSON.stringify({ challengeCode: _challenge.challenge_code });
                            broker.publish(message, function() {
                                logger.info('[' + pkg.name.toUpperCase() + ']', 'Publish:', JSON.stringify(message));
                            });
                        });
                    }
                    break;
                default:
                    break;
            }
        }
    } catch (err) {
        logger.error(err);
    }
}
