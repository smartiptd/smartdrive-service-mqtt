'use strict';

/* Module dependencies */
var mosca = require('mosca');
var mqttRegex = require('mqtt-regex');
var logger = require('smartdrive-lib').logger;
var pkg = require('./package');
var auth = require('./auth');
var config = require('config'),
    conf = config.get('app');

var broker;

/**********************
* MQTT broker process *
**********************/
var process = function() {
    try {
        auth.getToken(function(err, _token) {
            if (err) {
                logger.error(err);
            } else {
                if (_token) {
                    if (!broker) {

                        /*********
                        * Broker *
                        *********/
                        var ascoltatore = {
                            type: 'amqp',
                            json: false,
                            amqp: require('amqp'),
                            exchange: conf.broker.ascoltatore.amqp.exchange
                        };

                        broker = new mosca.Server({
                            interfaces: conf.broker.interfaces,
                            backend: ascoltatore
                        });

                        // broker.token = _token;
                        broker.authenticate = auth.clientAuthenticate();
                        broker.authorizePublish = auth.clientAuthorizePublish();
                        broker.authorizeSubscribe = auth.clientAuthorizeSubscribe();

                        /* Broker event */
                        broker.on('error', function(err) {
                            logger.error(err);
                            broker = null;
                            brokerProcess();
                        });

                        broker.on('clientConnected', function(client) {
                            logger.info('[CLIENT:' + client.id + ']', 'Connected');
                        });

                        broker.on('published', function(packet, client) {
                            if (packet && client) {
                                logger.info('[' + pkg.name.toUpperCase() + ']', 'Receive:', JSON.stringify({
                                    'topic': packet.topic,
                                    'payload': JSON.parse(packet.payload.toString())
                                }));
                                /* Identify package */
                                var pattern = '/+subsystype/+subsysid/#path';
                                var compiled = mqttRegex(pattern);
                                var executed = compiled.regex.exec(packet.topic);
                                var info = compiled.getParams(executed);
                                if (info) {
                                    switch (info.subsystype) {
                                        case 'common':
                                            require('./execute/common')(broker, client, packet, info);
                                            break;
                                        case 'aaf':
                                            require('./execute/aaf')(broker, client, packet, info);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        });

                        broker.on('subscribed', function(topic, client) {
                            //TODO add feature
                        });

                        broker.on('clientDisconnected', function(client) {
                            logger.info('[CLIENT:' + client.id + ']', 'Disconnected');
                        });

                        /* Start broker */
                        broker.on('ready', function() {
                            logger.info('[' + pkg.name.toUpperCase() + ']', 'Listening on port',
                                        conf.broker.interfaces[0].port, 'and',
                                        conf.broker.interfaces[1].port);
                        });

                    }
                    else if (broker.token != _token) {
                        broker.token = _token;
                    }
                }
            }
        });
        setTimeout(process, conf.broker.updateInfo * 1000);
    } catch (err) {
        logger.error(err);
    }
}

module.exports.process = process;
