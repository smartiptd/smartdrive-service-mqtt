'use strict';

/* Module dependencies */
var e_num = require('smartdrive-lib').enum;
var async = require('async');
var request = require('request');
var cache = require('../cache');
var config = require('config'),
    conf = config.get('app');

var exports = module.exports = {};

/*********************************
* Authentication & Authorization *
*********************************/

/* Exported functions */
exports.clientAuthenticate = function() {
    return function(client, username, password, callback) {
        try {
            var clientID = client.id;
            var clientAccessToken = username;
            var clientChallengeCode = password.toString();
            var options = {
                method: 'GET',
                url: conf.oauth.uri.tokenInfo,
                rejectUnauthorized: conf.oauth.options.rejectUnauthorized,
                qs: { access_token:clientAccessToken },
                headers:{ 'cache-control': 'no-cache' }
            };
            request(options, function (err, res, body) {
                if (err) {
                    return callback(err);
                } else {
                    var _body = JSON.parse(body);
                    if (!_body || !_body.expires_in) {
                        return callback(null, false);
                    } else {
                        var clientType = _body.type;
                        var subsys = _body.subsys;
                        var scope = _body.scope;
                        var expireDate = new Date().getTime() + (_body.expires_in * 1000);
                        if (new Date().getTime() > expireDate) {
                            return callback(null, false);
                        } else {
                            if (clientType == e_num.oauthDeviceType.GTW) {
                                var options = {
                                    method: 'GET',
                                    url: conf.oauth.uri.challengeClient,
                                    rejectUnauthorized: conf.oauth.options.rejectUnauthorized,
                                    qs: {
                                        access_token: clientAccessToken,
                                        challenge_code: clientChallengeCode
                                    },
                                    headers: { 'cache-control': 'no-cache' }
                                };
                                request(options, function (err, res, body) {
                                    if (err) {
                                        return callback(err);
                                    } else {
                                        var _body = JSON.parse(body);
                                        if (_body.result) {
                                            var _client = {
                                                clientID: clientID,
                                                token: clientAccessToken,
                                                clientType: clientType,
                                                subsys: subsys,
                                                scope: scope,
                                                expireDate: expireDate
                                            }
                                            cache.Client.save(clientID, _client, function(err) {
                                                if (err) {
                                                    return callback(err);
                                                } else {
                                                    return callback(null, true);
                                                }
                                            });
                                        } else {
                                            return callback(null, false);
                                        }
                                    }
                                });
                            } else {
                                var _client = {
                                    clientID: clientID,
                                    token: clientAccessToken,
                                    clientType: clientType,
                                    subsys: subsys,
                                    scope: scope,
                                    expireDate: expireDate
                                }
                                cache.Client.save(clientID, _client, function(err) {
                                    if (err) {
                                        return callback(err);
                                    } else {
                                        return callback(null, true);
                                    }
                                });
                            }
                        }
                    }
                }
            });
        } catch (err) {
            return callback(err);
        }
    }
}

exports.clientAuthorizePublish = function() {
    return function(client, topic, payload, callback) {
        try {
            if (client) {
                cache.Client.find(client.id, function(err, _client) {
                    if (err) {
                        return callback(err);
                    } else {
                        if (!_client || !_client.scope) {
                            return callback(null, false);
                        } else {
                            async.some(_client.scope, function(_scope, callback) {
                                var scopeRegex = getTopicRegex(_scope);
                                callback(scopeRegex.test(topic));
                            }, function(result) {
                                callback(null, result);
                            });
                        }
                    }
                });
            } else {
                return callback(null, false);
            }
        } catch (err) {
            return callback(err);
        }
    }
}

exports.clientAuthorizeSubscribe = function() {
    return function(client, topic, callback) {
        try {
            if (client) {
                cache.Client.find(client.id, function(err, _client) {
                    if (err) {
                        return callback(err);
                    } else {
                        if (!_client || !_client.scope) {
                            return callback(null, false);
                        } else {
                            async.some(_client.scope, function(_scope, callback) {
                                var scopeRegex = getTopicRegex(_scope);
                                callback(scopeRegex.test(topic));
                            }, function(result) {
                                callback(null, result);
                            });
                        }
                    }
                });
            } else {
                return callback(null, false);
            }
        } catch (err) {
            return callback(err);
        }
    }
}

/* Private functions */
function getTopicRegex(topic) {
    topic = topic.replace(/[\/]/g, '\\$&');
    topic = topic.replace(/[\+]/g, '\\w+');
    topic = topic.replace(/[#]/g, '\.*');
    return new RegExp(topic);
}
