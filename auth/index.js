'use strict';

/* Module dependencies */
var logger = require('smartdrive-lib').logger;
var pkg = require('../package');
var token = require('./token');
var client = require('./client');

var exports = module.exports = {};

/* Exported function */
exports.getToken = function(done) {
    try {
        token.readToken(function(err, _token) {
            if (err) {
                return done(err);
            } else {
                if (!_token) {
                    token.newToken(function(err, _token) {
                        if (err) {
                            return done(err);
                        } else {
                            if (!_token) {
                                logger.warn('[' + pkg.name.toUpperCase() + ']', 'No token for authentication');
                                return done(null, false);
                            } else {
                                logger.info('[' + pkg.name.toUpperCase() + ']', 'Get new token');
                                return done(null, _token);
                            }
                        }
                    });
                } else {
                    if (new Date().getTime() > _token.expires_date) {
                        logger.warn('[' + pkg.name.toUpperCase() + ']', 'Token expired');
                        token.updateToken(_token.refresh_token, function(err, _token) {
                            if (err) {
                                return done(err);
                            } else {
                                if (!_token) {
                                    logger.warn('[' + pkg.name.toUpperCase() + ']', 'No token for authentication');
                                    return done(null, false);
                                } else {
                                    logger.info('[' + pkg.name.toUpperCase() + ']', 'Update token with refresh token');
                                    return done(null, _token);
                                }
                            }
                        });
                    } else {
                        logger.info('[' + pkg.name.toUpperCase() + ']', 'Read old token');
                        return done(null, _token);
                    }
                }
            }
        });
    } catch (err) {
        return done(err);
    }
}

exports.removeToken = token.removeToken;

exports.clientAuthenticate = function() {
    return client.clientAuthenticate();
}
exports.clientAuthorizePublish = function() {
    return client.clientAuthorizePublish();
}
exports.clientAuthorizeSubscribe = function() {
    return client.clientAuthorizeSubscribe();
}
