'use strict';

/* Clients in memory data structure which stores all of the clients */
var clients = [];

module.exports = {

    find: function(clientID, done) {
        try {
            var _client = clients[clientID];
            return done(null, _client);
        } catch (err) {
            return done(err);
        }
    },

    save: function(clientID, _client, done) {
        try {
            clients[clientID] = _client;
            return done(null, _client);
        } catch (err) {
            return done(err);
        }
    },

    delete: function(clientID, done) {
        try {
            delete clients[clientID];
            return done(null);
        } catch (err) {
            return done(err);
        }
    }
}
